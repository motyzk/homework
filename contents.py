import pathlib
import os


def foo():
    folders = ['/.']
    while folders:
        x = pathlib.Path(folders.pop())
        os.chdir(x)
        for file_or_dir in x.iterdir():
            if not file_or_dir.is_dir():
                if 'txt' == str(file_or_dir).split('.')[-1]:
                    print(file_or_dir)
            else:
                os.chdir(file_or_dir)
                folders.append(pathlib.Path.cwd())
                os.chdir(x)


foo()
